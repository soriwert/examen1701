<?php
/**
 * Description of Bootstrap
 * Clase raíz que carga el controlador solicitado y ejecuta el método que se
 * haya pedido, con o sin argumentos.
 *
 * @rafacabeza
 */
class Bootstrap
{
    public function __construct()
    {
        session_start();

        if (isset($_GET['url'])) {
            $url = $_GET['url'];
        } else {
            $url = 'home';
        }
        $url = explode('/', $url);
        $controllerName = array_shift($url);
        $controllerName = ucfirst($controllerName) . 'Controller';
        if (count($url) > 0) {
            $method = array_shift($url);
            if ($method == '') {
                $method = 'index';
            }
        } else {
            $method = 'index';
        }


        try {
            $file = "controllers/$controllerName.php";
            if (!file_exists($file)) {
                throw new Exception("No encontrado", 404);
            }
            require_once $file;
            $controller = new $controllerName;

            if (!method_exists($controllerName, $method)) {
                throw new Exception("No encontrado", 404);
            }
            call_user_func_array(array($controller, $method), $url);
        } catch (Exception $e) {
            require_once "controllers/ErrorController.php";
            $controller = new ErrorController;
            $controller->index($e);
        }

        // var_dump($url);


        // LO SIGUIENTE ES UNA SOLUCIÓN "CUTRE" DE LO
        // QUE HACE LA FUNCIÓN ANTERIOR
        // switch (count($args)) {
        //     case 1:
        //         $controller->$method($args[0]);
        //         break;

        //     case 2:
        //         $controller->$method($args[0], $args[1]);
        //         break;

        //     default:
        //         $controller->$method();
        //         break;
        // }
    }
}
