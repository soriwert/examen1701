CREATE DATABASE IF NOT EXISTS `examen1701` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `examen1701`;


DROP TABLE IF EXISTS `producto`;
DROP TABLE IF EXISTS `tipo`;

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `nombre` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tipo`(`id`, `nombre`) VALUES
(1, 'perifericos'),
(2, 'almacenamiento'),
(3, 'consumibles');

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` decimal(6,2) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_tipo` int(11),
  index(id_tipo),
   FOREIGN KEY(id_tipo) references tipo(id) ON DELETE RESTRICT on UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `producto` (`id`, `nombre`, `precio`, `fecha`, `id_tipo`) VALUES
(1, 'raton R1', '10.50', '2017-11-11', 1),
(2, 'teclado T1', '12.39', '2017-11-12', 1),
(3, 'disco duro D3 1TB', '56.00', '2017-11-13', 2),
(4, 'pendrive P4', '1.80', '2017-11-14', 2),
(5, 'tonner T6', '20.33', '2017-11-15', 3);

