<?php require 'views/header.php'; ?>
<main>
   <table >
       <tr>
           <td>id</td>
           <td>nombre</td>
           <td>precio</td>
           <td>fecha</td>
           <td>id_tipo</td>
           <td>acciones</td>
       </tr>
       <tr>
           <?php foreach ($products as $product): ?>
               <td><?php echo $product->id ?></td>
               <td><?php echo $product->nombre ?></td>
               <td><?php echo $product->precio ?></td>
               <td><?php echo $product->fecha ?></td>
               <td><?php echo $product->id_tipo ?></td>
               <td><a href="/product/edit/<?php echo $product->id ?>">Editar</a></td>
               <td><a href="/product/remember/<?php echo $product->id ?>">Recordar</a></td>
           </tr>
       <?php endforeach ?>
       <form method="post" action="/product/create">
           <tr>
               <td><a href="/product/create">Crear Nuevo Libro</a></td>
           </tr>
       </form>
   </table>
</main>
<?php require 'views/footer.php'; ?>
