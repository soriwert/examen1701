<?php
require_once "models/Product.php";
class ProductController
{

    function __construct()
    {
    }
    public static function index()
    {
        $products = Product::all();
        require("views/products/index.php");
    }

    public function create()
    {
        include("views/products/create.php");
    }

    public function insert()
    {

        $product = new Product();
        $product->id = $_POST['id'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        $product->id_tipo = $_POST['id_tipo'];
        $product->store();
        header('location: index');
    }

    public function edit($id)
    {
        $product = Product::findById($id);
        include("views/products/edit.php");
    }

    public function update($id)
    {
        //buscar registro
        $product = Product::findById($id);
        //actualizar campos
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        $product->id_tipo = $_POST['id_tipo'];
        $product->save();
        header("location: ../index");

    }

    public function remember($id)
    {
        // session_destroy();
        // die('2');
        $product = Product::findbyId($id);
        $_SESSION['name'][] = $product->nombre;
        header('Location: /product');
    }
}
