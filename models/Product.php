<?php
require_once "app/Model.php";

class Product
{
    public $id;
    public $nombre;
    public $precio;
    public $fecha;
    public $id_tipo;

    function __construct()
    {
    }
    public static function all()
    {

        $db = Model::connect();
        $stmt = $db->prepare("SELECT * FROM producto");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');

        $results = $stmt->fetchAll();
        //var_dump($results);
        return $results;
    }

    public function store()
    {
        $db = Model::connect();
        $sql = "INSERT INTO producto(id, nombre, precio, fecha, id_tipo) VALUES(?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $stmt->bindParam(2, $this->nombre);
        $stmt->bindParam(3, $this->precio);

        $date = new DateTime($this->fecha);
        $stmt->bindParam(4, $date->format('Y-m-d H:i:s'));
        $stmt->bindParam(5, $this->id_tipo);
        $result = $stmt->execute();
    }

    public function findById($id)
    {
        $db = Model::connect();
        $sql = "SELECT * FROM producto WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $result = $stmt->fetch();
        return $result;
    }

     public function save()
    {
        $db = Model::connect();
        $sql = "UPDATE producto SET nombre=?, precio=?, fecha=?, id_tipo=? WHERE id=?";
        $stmt = $db->prepare($sql);

        $stmt->bindParam(1, $this->nombre);
        $stmt->bindParam(2, $this->precio);
        $stmt->bindParam(3, $this->fecha);
        $stmt->bindParam(4, $this->id_tipo);
        $stmt->bindParam(5, $this->id);
        $result = $stmt->execute();
        return $result;
    }
}
